package com.tahir.bookStore.Entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    String title;

    String description;

    String author;

    String isbn;

    int printYear;

    boolean readAlready;


    String  imageStr;

    @Lob
    @Column(name="image_data")
    private byte[] imageData;
}


