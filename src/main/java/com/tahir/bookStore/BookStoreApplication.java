package com.tahir.bookStore;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@EnableSwagger2
@SpringBootApplication
public class BookStoreApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(BookStoreApplication.class, args);
    }

}
