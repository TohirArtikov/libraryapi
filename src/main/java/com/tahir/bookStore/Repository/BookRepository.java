package com.tahir.bookStore.Repository;

import com.tahir.bookStore.Entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    @Query("SELECT book FROM Book book WHERE " +
            "(LOWER(book.title) LIKE LOWER(CONCAT('%', :searchTerm, '%')) OR " +
            "LOWER(book.author) LIKE LOWER(CONCAT('%', :searchTerm, '%')) OR " +
            "LOWER(book.description) LIKE LOWER(CONCAT('%', :searchTerm, '%'))) " +
            "AND book.printYear > :printYear")
    Page<Book> findBySearchParams(
            @Param("searchTerm") String searchTerm,
            @Param("printYear") int printYear,
            Pageable pageRequest
    );

    @Query("SELECT book FROM Book book WHERE " +
            "(LOWER(book.title) LIKE LOWER(CONCAT('%', :searchTerm, '%')) OR " +
            "LOWER(book.author) LIKE LOWER(CONCAT('%', :searchTerm, '%')) OR " +
            "LOWER(book.description) LIKE LOWER(CONCAT('%', :searchTerm, '%'))) " +
            "AND book.printYear > :printYear AND book.readAlready = :readAlReady")
    Page<Book> findBySearchParamsAndReadAlready(
            @Param("searchTerm") String searchTerm,
            @Param("printYear") int printYear,
            @Param("readAlReady") boolean readAlReady,
            Pageable pageRequest
    );

}

